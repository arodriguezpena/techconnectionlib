export declare interface IResponse<T> {
  success: boolean;
  message?: string;
  data?: T;
}

export declare function login(email: string, password: string): Promise<IResponse<{token:string}>>;
export declare function getUsers(delay: number): Promise<IResponse<any[]>>;
